import java.util.Scanner;

public class Main {

    public static int inputNumber(){
        Scanner scanner = new Scanner(System.in);
        return  scanner.nextInt();
    }

    public static void main(String[] args) {

        Test1 test = new Test1();

        test.variableOne = inputNumber();
        test.variableTwo = inputNumber();

        test.displayInfo();

    }
}
class Test1 {
    int variableOne;
    int variableTwo;

    int sumFinder() {
        return variableOne + variableTwo;
    }

    int largestFinder(){
        int a = variableOne;
        int b = variableTwo;
        if(a <= b){
            return b;
        }else{
            return a;
        }
    }

    void displayInfo(){
        System.out.println("You have entered " + variableOne + " and " + variableTwo);
        System.out.println("Their sum is " + sumFinder());
        System.out.println("The largest number is " + largestFinder());
    }
}